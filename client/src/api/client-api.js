import axios from "axios";

export class Api {
    constructor() {
        this._msgpack = require("msgpack5")();
        axios.defaults.baseURL = window.location.origin;
        axios.defaults.headers.post["Content-Type"] = "application/octet-stream";
        axios.defaults.responseType = "arraybuffer";
    }

    getRepos() {
        return axios
            .get("/api/get/objects")
            .then(res => {
                return Promise.resolve(this._msgpack.decode(res.data));
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

    getReposList() {
        return axios
            .get("/api/get/list")
            .then(res => {
                return Promise.resolve(this._msgpack.decode(res.data));
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

    getRepoRemotes(repo) {
        return axios
            .get("/api/get/remote/" + repo)
            .then(res => {
                return Promise.resolve(this._msgpack.decode(res.data));
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

    getRepoLastModTime(repo) {
        return axios
            .get("/api/get/lmt/" + repo)
            .then(res => {
                return Promise.resolve(this._msgpack.decode(res.data));
            })
            .catch(err => {
                return Promise.reject(err);
            });
    }

    createNewRepo(repo) {
        return axios
            .post("/api/create/repo/" + repo)
            .then(_res => {
                return Promise.resolve();
            })
            .catch(err => {
                return Promise.reject(
                    this._msgpack.decode(err.response.data).error
                );
            })
    }
}
