import React, { Component } from "react";
import { List, Grid, Input } from "semantic-ui-react";

class RepoListItem extends Component {
  state = { copied: false, copyActionColor: 'gray' };

  handleCopyActionClick = () => {
    const copy = require('copy-text-to-clipboard');
    copy(this.props.item.remote);

    this.setState({
      inputValue: 'Copied!',
      copyActionColor: 'olive'
    });

    this.timeout = setTimeout(() => {
      this.setState({
        inputValue: this.props.item.remote,
        copyActionColor: 'gray'
      });
      clearTimeout(this.timeout);
    }, 500);

  }

  componentWillMount() {
    this.setState({
      inputValue: this.props.item.remote
    });
  }

  render() {
    return (
      <List.Item>
        <List.Icon name="folder" size="large" verticalAlign="middle" />
        <List.Content>
          <Grid columns={2} relaxed="very" stackable>
            <Grid.Column>
              <List.Header>{this.props.item.name}</List.Header>
              <List.Description>{this.props.item.lastMod}</List.Description>
            </Grid.Column>
            <Grid.Column textAlign="right">
              <Input
                ref={this.contextRef}
                value={this.state.inputValue}
                size='mini'
                fluid
                action={{
                  color: this.state.copyActionColor,
                  icon: 'copy',
                  onClick: () => this.handleCopyActionClick()
                }}
              />
            </Grid.Column>
          </Grid>
        </List.Content>
      </List.Item>
    );
  }
}

export default RepoListItem;
