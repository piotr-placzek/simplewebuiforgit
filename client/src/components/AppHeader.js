import React from "react";
import { Container, Menu, Icon } from "semantic-ui-react";

import CreateRepoModal from "./CreateRepoModal";

const AppHeader = () => {
  return (
    <Menu fixed="top" color="brown" inverted>
      <Container>
        <Menu.Item header>
          {/* <Image
            size="mini"
            src="https://api.adorable.io/avatars/64/SGWU.png"
            style={{ marginRight: "1.5em" }}
          /> */}
          <Icon name="code branch" size="big"/>
          Simple Web UI for Git
          <br />
          by pplaczek.pl
        </Menu.Item>
        <Menu.Item>
          <CreateRepoModal />
        </Menu.Item>
      </Container>
    </Menu>
  );
};

export default AppHeader;
