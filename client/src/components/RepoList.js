import React, { Component } from "react";
import { List, Message } from "semantic-ui-react";
import RepoListItem from "./RepoListItem";

class RepoList extends Component {

  render() {
    if (this.props.items.length > 0) {
      return (
        <List divided relaxed>
          {this.props.items.map(item => (
            <RepoListItem item={item} />
          ))}
        </List>
      );
    } else {
      return (
        <Message>
          <Message.Header>Nothing to show</Message.Header>
          <p>
            There are no repositories.<br />
            Create new one or check is the api's server works.
          </p>
        </Message>
      );
    }
  }
}

export default RepoList;
