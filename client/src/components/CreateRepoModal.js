import React, { Component, createRef } from "react";
import { connect } from 'react-redux'
import { Button, Header, Icon, Modal, Input, Form, Label } from "semantic-ui-react";
import { fetchRepositories } from "../store/actions/fetchRepositories";

const mapStateToProps = (state) => {
  return { ...state };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchRepositories: () => dispatch(fetchRepositories()),
  };
};

class CreateRepoModal extends Component {
  state = {
    modalOpen: false,
    name: "",
    waiting: false,
    error: false,
    error_text: ""
  };

  inputRef = createRef();

  componentDidUpdate() {
    if(this.state.modalOpen) {
      this.inputRef.current.focus();
    }
  }

  handleOpen = () => this.setState({ modalOpen: true });

  handleClose = () => this.setState({
    modalOpen: false,
    name: "",
    waiting: false,
    error: false,
    error_text: ""
  });

  // eslint-disable-next-line
  inputChangeHandler = (event, data) => {
    this.setState({ name: data.value });
  }

  createNewRepo = () => {
    this.setState({ waiting: true });
    const Api = require("../api/client-api.js").Api;
    const api = new Api();
    api
      .createNewRepo(this.state.name)
      .then(() => {
        this.props.fetchRepositories();
        this.handleClose();
      })
      .catch(error => {
        this.setState({
          waiting: false,
          error: true,
          error_text: error
        });
      });
  };

  render() {
    let errorLabel = "";
    if (this.state.error) {
      errorLabel = (
        <Label basic color='red' pointing active={false}>
          {this.state.error_text}
        </Label>
      );
    }

    return (
      <Modal
        trigger={
          <Button onClick={this.handleOpen}>
            <Icon name="plus" />
            Create
          </Button>
        }
        open={this.state.modalOpen}
        onClose={this.handleClose}
        closeOnEscape={true}
        closeOnDimmerClick={false}
        basic
        size="small"
      >
        <Header icon="git square" content="Create new repository" />
        <Modal.Content>
          <Form onSubmit={this.createNewRepo}>
            <Form.Field>
              <Input ref={this.inputRef} fluid placeholder="Type new repo's name here" onChange={this.inputChangeHandler} error={this.state.error} />
              {errorLabel}
            </Form.Field>
          </Form>
        </Modal.Content>
        <Modal.Actions>
          <Button basic color="red" onClick={this.handleClose} inverted disabled={this.state.waiting}>
            <Icon name="remove" /> Cancel
          </Button>
          <Button color="green" onClick={this.createNewRepo} inverted loading={this.state.waiting} disabled={this.state.waiting}>
            <Icon name="checkmark" /> Create
          </Button>
        </Modal.Actions>
      </Modal>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateRepoModal);
