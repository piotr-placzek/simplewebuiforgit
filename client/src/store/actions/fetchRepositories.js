
const repositoriesFetched = repositories => ({
    type: "REPOSITORIES_FETCHED",
    repositories
});

const fetchingRepositoriesFailed = error => ({
    type: "FETCHING_REPOSITORIES_FAILED",
    error
});

export const fetchRepositories = () => (dispatch) => {
    const Api = require("../../api/client-api").Api;
    const api = new Api();

    return api.getRepos()
        .then(list => {
            dispatch(repositoriesFetched(list));
        })
        .catch(error => {
            console.debug(error);
            dispatch(fetchingRepositoriesFailed(error));
        });
};