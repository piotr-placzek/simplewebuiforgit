export default (state = { repositories: [] }, action) => {
    switch (action.type) {
        case "REPOSITORIES_FETCHED":
            {
                console.info("REPOSITORIES_FETCHED");
                return { ...state, repositories: action.repositories };
            }
        default:
            return state;
    };
};