import React, { Component } from "react";
import "./styles.css";

import { Container } from "semantic-ui-react";
import AppHeader from "./components/AppHeader";
import RepoList from "./components/RepoList";

import { connect } from 'react-redux'
import { fetchRepositories } from "./store/actions/fetchRepositories";

class App extends Component {

  componentWillMount() {
    this.props.fetchRepositories();
  }

  render() {
    return (
      <div className="App">
        <AppHeader />
        <Container text style={{ marginTop: "7em" }}>
          <RepoList items={this.props.repositories} />
        </Container>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  repositories: state.repositories
});

export default connect(mapStateToProps, {
  fetchRepositories: fetchRepositories
})(App);