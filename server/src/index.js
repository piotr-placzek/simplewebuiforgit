const fs = require("fs");

fs.readFile("config.json", (err, res) => {
    if (err) throw err;

    const express = require("express");
    const bodyParser = require("body-parser");
    const api = require("./api/server-api").API;

    const CONFIG = JSON.parse(res);

    const app = express();
    app.use(bodyParser.raw());
    app.use(express.static(CONFIG.static));

    api.build(app, CONFIG);

    if (CONFIG.cors.enabled) {
        const cors = require("cors");
        app.use(cors());
    }

    if (CONFIG.http.enabled) {
        const http = require("http");

        const httpServer = http.createServer(app);
        httpServer.listen(CONFIG.http.port, CONFIG.http.host);

        console.log("HTTP  server started at  http://"
            + CONFIG.http.host + ":"
            + CONFIG.http.port
        );
    }

    if (CONFIG.https.enabled) {
        const https = require("https");
        
        let httpsOptions = { key: "", cert: "" };

        fs.readFile(CONFIG.https.tsl.key, (err, res) => {
            if (err) throw err;
            httpsOptions.key = res;

            fs.readFile(CONFIG.https.tsl.crt, (err, res) => {
                if (err) throw err;
                httpsOptions.cert = res;
            });

            const httpsServer = https.createServer(httpsOptions, app);
            httpsServer.listen(CONFIG.https.port, CONFIG.https.host);

            console.log("HTTPS server started at https://"
                + CONFIG.https.host + ":"
                + CONFIG.https.port
            );
        });
    }
});
