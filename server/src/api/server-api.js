const msgpack = require("msgpack5")();

const getRepositoriesNames = (path) => {
    try {
        const fs = require("fs");
        let items = fs.readdirSync(path);
        items.forEach((item, index) => {
            if (item.endsWith(".git")) {
                items[index] = item.slice(0, -4);
            }
        });
        return items;
    } catch (err) {
        console.error(err);
        return null;
    }
}

const getRepositoryLastModifyTime = (path) => {
    try {
        const fs = require("fs");
        const stat = fs.statSync(path);
        return stat.mtime.toLocaleString();
    } catch (err) {
        console.error(err);
        return null;
    }
}

module.exports.API = {
    build: (app, cfg) => {

        const host = (req) => {
            return req.req.headers.host.split(":")[0];
        };

        const remote = (req, repo) => {
            return {
                git: cfg.git.user + "@" + host(req) + ":" + cfg.git.path + repo + ".git",
                // ssh: "ssh://" + cfg.git.user + "@" + host(req) + ":23" + cfg.git.path + repo + ".git/"
            };
        };

        app.get("/api/get/objects", (res, req) => {
            const names = getRepositoriesNames(cfg.git.path);
            if (names === null) {
                res.res.sendStatus(500);
            }
            else {
                let objects = Array();
                names.forEach(name => {
                    const lmt = getRepositoryLastModifyTime(cfg.git.path + name + ".git");
                    const remotes = remote(req, name);
                    objects.push({
                        name: name,
                        lastMod: lmt,
                        remote: remotes.git
                        // ssh: remotes.ssh,
                        // git: remotes.git
                    });
                });
                res.res.send(msgpack.encode(objects));
            }
        });

        app.get("/api/get/list", (res, _req) => {
            const fs = require("fs");
            fs.readdir(cfg.git.path, (err, items) => {
                if (err) {
                    console.error(err);
                    res.res.sendStatus(500);
                }
                else {
                    items.forEach((item, index) => {
                        if (item.endsWith(".git")) {
                            items[index] = item.slice(0, -4);
                        }
                    });
                    res.res.send(msgpack.encode(items));
                }
            })
        });

        app.get("/api/get/remote/:repo", (res, req) => {
            const repo = req.req.params.repo;
            res.res.send(
                msgpack.encode(
                    remote(req, repo)
                )
            );
        });

        app.get("/api/get/lmt/:repo", (res, req) => {
            const fs = require("fs");
            const repo = req.req.params.repo;
            const path = cfg.git.path + repo + ".git";

            fs.stat(path, (err, stats) => {
                if (err) {
                    console.error(err);
                    res.res.sendStatus(500);
                }
                else {
                    res.res.send(
                        msgpack.encode({
                            name: repo,
                            lastMod: stats.mtime
                        })
                    );
                }
            });
        });

        app.post("/api/create/repo/:repo", (res, req) => {
            const fs = require("fs");
            const repo = req.req.params.repo;
            const path = cfg.git.path + repo + ".git/";

            try {
                fs.accessSync(path);
                // if acces is ok then return conflict and do not create new repository
                res.res.status(409).send(
                    msgpack.encode({
                        error: "Directory with this name already exists."
                    })
                );
                return;

            } catch (err) {
                // if acces is not ok then try to create new directory
                try {
                    fs.mkdirSync(path)
                } catch (err) {
                    console.error(err);
                    res.res.status(500).send(
                        msgpack.encode({
                            error: "Can not create new directory."
                        })
                    );
                    return;
                }
            }

            try {
                const simpleGit = require('simple-git')(path);
                simpleGit.init(true, () => {
                    simpleGit.addRemote("origin", remote(req, repo).ssh);

                    fs.chown(path, cfg.owner.uid, cfg.owner.gid);

                    res.res.sendStatus(201);
                });
            } catch (err) {
                console.error(err);
                res.res.status(500).send(
                    msgpack.encode({
                        error: "Can not initialize new repository."
                    })
                );
            }
        });
    }
}
